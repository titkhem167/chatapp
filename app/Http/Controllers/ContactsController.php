<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Message;
use App\Events\NewMessage;

class ContactsController extends Controller
{
    public function index(){
        $user = User::find(auth()->id());
        $contacts = $user->getFriends();
        return response()->json($contacts);
    }

}
